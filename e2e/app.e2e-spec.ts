import { AngularHttpObservablesPage } from './app.po';

describe('angular-http-observables App', function() {
  let page: AngularHttpObservablesPage;

  beforeEach(() => {
    page = new AngularHttpObservablesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
