import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Person } from '../model/person';
import { Vehicle } from '../model/vehicle';
import { Starship } from '../model/starship';

@Injectable()
export class SwapiService {

  constructor(public http: Http) { }

  /*========================
            people
    ========================*/

  getPerson(id :number): Promise<Person> {
    return this.http
      .get('http://swapi.co/api/people/' + id)
      .toPromise()
      .then(data => data.json() as Person)
      .catch(this.logError);
  }

  getPeoplePage(url?:string): Promise<Person[]> {
    if(!url)
     url = 'http://swapi.co/api/people/';

    return this.http
      .get(url)
      .toPromise()
      .then(data => data.json() as Person[])
      .catch(this.logError);
  }

  /*========================
            vehicles
    ========================*/

  getVehicle(id :number): Promise<Vehicle> {
    return this.http
      .get('http://swapi.co/api/vehicles/' + id)
      .toPromise()
      .then(data => data.json() as Vehicle)
      .catch(this.logError);
  }

  getVehiclePage(url?:string): Promise<Vehicle[]> {
    if(!url)
     url = 'http://swapi.co/api/vehicles/';

    return this.http
      .get(url)
      .toPromise()
      .then(data => data.json() as Vehicle[])
      .catch(this.logError);
  }

  /*========================
            starships
    ========================*/

  getStarship(id :number): Promise<Starship> {
    return this.http
      .get('http://swapi.co/api/starships/' + id)
      .toPromise()
      .then(data => data.json() as Vehicle)
      .catch(this.logError);
  }

  getStarshipPage(url?:string): Promise<Starship[]> {
    if(!url)
     url = 'http://swapi.co/api/starships/';

    return this.http
      .get(url)
      .toPromise()
      .then(data => data.json() as Starship[])
      .catch(this.logError);
  }

  logError(err :any) {
    console.error('There was an error: ' + err);
  }

}
