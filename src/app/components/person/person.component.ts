import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Person } from '../../model/person';
import { SwapiService } from '../../services/swapi.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {
  person :Person;

  constructor(
    private swapiService: SwapiService,
    private route: ActivatedRoute,
    private router: Router) {
      this.person = new Person();
  }

  ngOnInit() {
    //get ID from route
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        let id = +params['id'];

        //send get to swapi
        this.swapiService.getPerson(id)
          .then(person => {
            this.person = person as Person;
          });
      } else {
        //error
        this.person = new Person();
      }
    });
  }

  gotoURL(url: string): void {
    //get id from person URL by splitting over /
    // e.g. ["http:", "", "swapi.co", "api", "people", "1", ""]
    let urlSplit = url.split("/");
    let id = urlSplit[urlSplit.length - 2];
    let page = '/' + urlSplit[urlSplit.length - 3];
    let link = [page, id];
    this.router.navigate(link);
  }
}
