import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Starship } from '../../model/starship';
import { SwapiService } from '../../services/swapi.service';

@Component({
  selector: 'app-starships',
  templateUrl: './starships.component.html',
  styleUrls: ['./starships.component.scss']
})
export class StarshipsComponent implements OnInit {

  starships :Starship[] = [];
  previous :string;
  next :string;

  constructor(
    private swapiService: SwapiService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.swapiService.getStarshipPage()
      .then(starships => {
        this.previous = starships['previous'];
        this.next = starships['next'];
        this.starships = starships['results'];
      });
  }

  getPage(url :string) {
    this.swapiService.getStarshipPage(url)
      .then(starships => {
        this.previous = starships['previous'];
        this.next = starships['next'];
        this.starships = starships['results'];
      });
  }

  gotoVehicle(starship: Starship): void {
    //get id from vehicle URL by splitting over /
    // e.g. ["http:", "", "swapi.co", "api", "vehicles", "1", ""]
    let urlSplit = starship['url'].split("/");
    let id = urlSplit[urlSplit.length-2];
    let link = ['/starships', id];
    this.router.navigate(link);
  }

}
