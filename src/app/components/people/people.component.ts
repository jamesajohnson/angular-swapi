import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Person } from '../../model/person';
import { SwapiService } from '../../services/swapi.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {

  people :Person[] = [];
  previous :string;
  next :string;

  constructor(
    private swapiService: SwapiService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.swapiService.getPeoplePage()
      .then(people => {
        this.previous = people['previous'];
        this.next = people['next'];
        this.people = people['results'];
      });
  }

  getPage(url :string) {
    this.swapiService.getPeoplePage(url)
      .then(people => {
        this.previous = people['previous'];
        this.next = people['next'];
        this.people = people['results'];
      });
  }

  gotoPerson(person: Person): void {
    //get id from person URL by splitting over /
    // e.g. ["http:", "", "swapi.co", "api", "people", "1", ""]
    let urlSplit = person['url'].split("/");
    let id = urlSplit[urlSplit.length-2];
    let link = ['/people', id];
    this.router.navigate(link);
  }

}
