import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Starship } from '../../model/starship';
import { SwapiService } from '../../services/swapi.service';

@Component({
  selector: 'app-starship',
  templateUrl: './starship.component.html',
  styleUrls: ['./starship.component.scss']
})
export class StarshipComponent implements OnInit {
  starship :Starship;

  constructor(
    private swapiService: SwapiService,
    private route: ActivatedRoute,
    private router: Router) {
      this.starship = new Starship();
  }

  ngOnInit() {
    //get ID from route
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        let id = +params['id'];

        //send get to swapi
        this.swapiService.getStarship(id)
          .then(starship => {
            this.starship = starship as Starship;
          });
      } else {
        //error
        this.starship = new Starship();
      }
    });
  }

  gotoURL(url: string): void {
    //get id from person URL by splitting over /
    // e.g. ["http:", "", "swapi.co", "api", "people", "1", ""]
    let urlSplit = url.split("/");
    let id = urlSplit[urlSplit.length - 2];
    let page = '/' + urlSplit[urlSplit.length - 3];
    let link = [page, id];
    this.router.navigate(link);
  }
}
