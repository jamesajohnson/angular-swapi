import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Vehicle } from '../../model/vehicle';
import { SwapiService } from '../../services/swapi.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {

  vehicles :Vehicle[] = [];
  previous :string;
  next :string;

  constructor(
    private swapiService: SwapiService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.swapiService.getVehiclePage()
      .then(vehicle => {
        this.previous = vehicle['previous'];
        this.next = vehicle['next'];
        this.vehicles = vehicle['results'];
      });
  }

  getPage(url :string) {
    this.swapiService.getVehiclePage(url)
      .then(vehicles => {
        this.previous = vehicles['previous'];
        this.next = vehicles['next'];
        this.vehicles = vehicles['results'];
      });
  }

  gotoVehicle(vehicle: Vehicle): void {
    //get id from vehicle URL by splitting over /
    // e.g. ["http:", "", "swapi.co", "api", "vehicles", "1", ""]
    let urlSplit = vehicle['url'].split("/");
    let id = urlSplit[urlSplit.length-2];
    let link = ['/vehicles', id];
    this.router.navigate(link);
  }

}
