import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import { SwapiService } from './services/swapi.service';
import { PersonComponent } from './components/person/person.component';
import { HomeComponent } from './components/home/home.component';
import { PeopleComponent } from './components/people/people.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { StarshipComponent } from './components/starship/starship.component';
import { StarshipsComponent } from './components/starships/starships.component';

const appRoutes: Routes = [
  { path: 'people', component: PeopleComponent },
  { path: 'people/:id', component: PersonComponent },
  { path: 'vehicles', component: VehiclesComponent },
  { path: 'vehicles/:id', component: VehicleComponent },
  { path: 'starships', component: StarshipsComponent },
  { path: 'starships/:id', component: StarshipComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PersonComponent,
    HomeComponent,
    PeopleComponent,
    VehicleComponent,
    VehiclesComponent,
    StarshipComponent,
    StarshipsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    SwapiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
