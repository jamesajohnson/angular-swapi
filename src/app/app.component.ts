import { Component } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

import { Person } from './model/person';
import { SwapiService } from './services/swapi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';
  person :Person;

  constructor(private swapiService: SwapiService) {
    this.person = new Person();
  }

  ngOnInit() {
    this.swapiService.getPerson(1)
      .then(person => {
        this.person = person as Person;
      });
  }
}
